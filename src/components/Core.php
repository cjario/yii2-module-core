<?php
namespace tmukherjee13\core\components;

use Yii;
use yii\base\Component;

class Core extends Component
{

    private $data = array();

    public static function getRealIP()
    {
        $ip = false;

        $seq = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');

        foreach ($seq as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }

    public static function isHome()
    {
        $controller         = Yii::$app->controller;
        $default_controller = Yii::$app->defaultRoute;
        $isHome             = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;
        return $isHome;
    }
    

}
