<?php
namespace tmukherjee13\core;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'tmukherjee13\core\controllers';
    
    public function init() {
        parent::init();
        
        // custom initialization code goes here
        
        // initialize the module with the configuration loaded from config.php
        // \Yii::configure($this, require (__DIR__ . '/config/config.php'));
        
    }
    
    public function bootstrap($app) {
        
        parent::bootstrap();
        $sFilePathConfig = __DIR__ . '/config/_routes.php';
        if (file_exists($sFilePathConfig)) {
            $app->getUrlManager()->addRules(require ($sFilePathConfig));
        }
    }
}
