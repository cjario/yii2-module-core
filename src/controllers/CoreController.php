<?php

namespace tmukherjee13\core\controllers;

ini_set('display_errors', '0');
error_reporting(E_ALL | E_STRICT);

//use backend\controllers\ProfileController;
use backend\modules\core\models\Core;
use backend\modules\core\models\MyEvent;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

class CoreController extends Controller
{
    protected $meta_title       = '';
    protected $meta_keywords    = '';
    protected $meta_description = '';

    protected $button_list       = '{view} {update} {delete}';
    protected static $isLoggedIn = false;
    protected static $userId     = 0;

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        // $behaviors['access'] = [
        //     'class' => AccessControl::className(),
        //     'rules' => [
        //         [
        //             'allow'   => true,
        //             'actions' => ['view', 'index'],
        //             'roles'   => ['viewContent', 'createContent'],
        //         ],
        //         [
        //             'allow'   => true,
        //             'actions' => ['create'],
        //             'roles'   => ['createContent'],
        //         ],
        //         [
        //             'allow'   => true,
        //             'actions' => ['update'],
        //             'roles'   => ['updateContent'],
        //         ],
        //         [
        //             'allow'   => true,
        //             'actions' => ['delete'],
        //             'roles'   => ['deleteContent'],
        //         ],
        //     ],
        // ];
        $behaviors['verbs'] = [
            'class'   => VerbFilter::className(),
            'actions' => [
                'delete' => ['post'],
            ],
        ];
        return $behaviors;
    }

    public function init()
    {
        parent::init();

        // $this->seo();
        // $this->check_permission();
        // $this->track_user_login();

        // $this->setDefaults();
    }

    /**
     * setDefaults function
     * Set the default values based on current store.
     *
     * @return void
     * @author Tarun Mukherjee
     **/
    public function setDefaults()
    {
    }

    public function track_user_login()
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->isGuest) {
            if ($session->get('isLoggedIn')) {
                self::$isLoggedIn = false;
                $session->set('isLoggedIn', self::$isLoggedIn);
                // $event = new MyEvent();
                // $event->trigger(MyEvent::EVENT_ADMIN_LOGOUT);
            }
        } else {
            if (!$session->get('isLoggedIn')) {
                self::$isLoggedIn = true;
                $session->set('isLoggedIn', self::$isLoggedIn);
                $event = new MyEvent();
                $event->trigger(MyEvent::EVENT_ADMIN_LOGIN);
            }
        }

        /* if (!Yii::$app->user->isGuest && !$session->get('isLoggedIn')) {
    self::$isLoggedIn = true;
    $session->set('isLoggedIn', self::$isLoggedIn);
    $event = new MyEvent();
    $event->trigger(MyEvent::EVENT_ADMIN_LOGIN);
    }*/
    }

    public function seo()
    {
        $this->meta_title = "Hello";
    }

    public static function slugify($text)
    {
        // Swap out Non "Letters" with a -
        $text = preg_replace('/[^\\pL\d]+/u', '-', $text);

        // Trim out extra -'s
        $text = trim($text, '-');

        // Convert letters that we have left to the closest ASCII representation
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // Make text lowercase
        $text = strtolower($text);

        // Strip out anything we haven't been able to convert
        $text = preg_replace('/[^-\w]+/', '', $text);

        return $text;
    }
}
