<?php

namespace tmukherjee13\core;

use yii;
use yii\base\BootstrapInterface;

/**
 * Class ModuleBootstrap
 *
 * @package app\modules\product
 */
class ModuleBootstrap implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {

        $moduleConfigs = yii\helpers\FileHelper::findFiles(realpath(Yii::getAlias('@app')), [
            'only'      => ['config.php'],
            // 'except' => ['core/'],
            'recursive' => true,
        ]);

        $moduleCConfigs = yii\helpers\FileHelper::findFiles(realpath(Yii::getAlias('@vendor/cjario')), [
            'only'      => ['config.php'],
            // 'except' => ['core/'],
            'recursive' => true,
        ]);


        $moduleConfigs = array_merge($moduleConfigs,$moduleCConfigs);

        foreach ($moduleConfigs as $value) {
            Yii::configure(\Yii::$app, require ($value));
        }
        
        $moduleCRoute = yii\helpers\FileHelper::findFiles(realpath(Yii::getAlias('@vendor/cjario')), [
            'only'      => ['_routes.php'],
            // 'except' => ['core/'],
            'recursive' => true,
        ]);

        foreach ($moduleCRoute as $_routes) {
        
            if (file_exists($_routes)) {
                    $app->getUrlManager()->addRules(require ($_routes));
                }
            
        }

        if (!defined('FILE_PATH_ROOT')) {
            // define('FILE_PATH_ROOT', realpath(dirname(dirname(dirname(__FILE__)))));
            define('FILE_PATH_ROOT', Yii::getAlias('@app'));
        }

        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }

        $aModuleList = $app->getModules();

        $migrationAdPath = [];
        foreach ($aModuleList as $sKey => $aModule) {
            if (is_array($aModule) && (strpos($aModule['class'], 'common\modules') === 0 || strpos($aModule['class'], 'backend\modules') === 0 || strpos($aModule['class'], 'frontend\modules') === 0)) {
                $sFilePath = FILE_PATH_ROOT . DS . 'modules' . DS . $sKey . DS . 'config' . DS;
                $sRoute    = $sFilePath . '_routes.php';
                $sConfig   = $sFilePath . 'config.php';

                /** Initialize Module Routes */
                if (file_exists($sRoute)) {
                    $app->getUrlManager()->addRules(require ($sRoute));
                }

                /** Initialize Module Configuration */
                if (file_exists($sConfig)) {
                    Yii::configure(Yii::$app, require ($sConfig));
                }

                }



               if(is_array($aModule) && strpos($aModule["class"], 'cj') !== false){
                    $migrationPath = str_replace(['\\','/Module'],['/','/migrations'],$aModule['class']);
                    array_push($migrationAdPath,'@'.$migrationPath);
                }
        }

        /** @var Yii::$app->params Params defined in config file */
        foreach (Yii::$app->params as $key => $value) {
            if (!defined(strtoupper($key))) {
                define(strtoupper($key), $value);
            }

        }


        /**
         * Dynamically add module migrations
         */
        if ($app instanceof yii\console\Application) {
            Yii::configure($app, [
                'controllerMap' => [
                    'migrate' => [
                        'class'           => 'tmukherjee13\core\console\controllers\MigrateController',
                        'migrationLookup' => $migrationAdPath
                    ],
                ],
            ]);
        }
    }
}
