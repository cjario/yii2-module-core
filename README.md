Yii2 Core Module
=================
Core Module to initialize other modules

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist tmukherjee13/yii2-module-core "*"
```

or add

```
"tmukherjee13/yii2-module-core": "*"
```

to the require section of your `composer.json` file.


